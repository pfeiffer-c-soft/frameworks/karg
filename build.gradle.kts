/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Kotlin library project to get you started.
 * For more details take a look at the 'Building Java & JVM projects' chapter in the Gradle
 * User Manual available at https://docs.gradle.org/8.1.1/userguide/building_java_projects.html
 */

plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "2.0.0"

    // Apply the java-library plugin for API and implementation separation.
    `java-library`

    // Apply the maven-publish plugin to add support for publishing to Maven Central.
    `maven-publish`

    // Apply the signing plugin to add support for signing JARs.
    `signing`
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    // Use the Kotlin JUnit 5 integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")

    // Use the JUnit 5 integration.
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.1")

    api("org.slf4j:slf4j-api:1.7.30")
    testImplementation("org.slf4j:slf4j-simple:1.7.30")
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

publishing {
    repositories {
        maven {
            name = "MyRepo"
            url = uri("https://repo.maven.apache.org/maven2")

            credentials {
                // Use placeholders, replace them with your actual username and password.
                username = "maven-username"
                password = "maven-password"
            }
        }
    }
    publications {
        create<MavenPublication>(project.name) {
            from(components["kotlin"])
            // Add necessary pom details
            pom {
                name.set("KArg")
                description.set("A framework, optimized for Kotlin and Java, to read arguments based on a profile.")
                url.set("https://gitlab.com/pfeiffer-c-soft/frameworks/karg.git")

                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
            }
        }
    }
}

// Configure the signing of the publication
signing {
    useInMemoryPgpKeys("Your PGP public key", "Your PGP private key", "Your key password")
    sign(publishing.publications[project.name])
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}