# KArg

This is a framework to read arguments on an easy way. This framework is Kotlin and Java optimized.
You can use it with a fluent API or the Kotlin builder pattern.

## Concepts

The concept based on argument reading based on groups and subgroups. So you can define "commands" with
own group of arguments. On the other hand you can define common arguments.

### Argument Types

There are 4 types of arguments:

<table>
<tr>
<td>Name</td>
<td>Description</td>
<td>Example</td>
</tr>
<tr>
<td>Command Argument</td>
<td>An argument defined by a fixed string to use as "command", see example below.</td>
<td><code>build</code> <code>run</code></td></tr>
<tr>
<td>Option Arguments</td>
<td>An argument to define an option (switch on or switch off) in short or long form. It starts with an option marker key like <code>-</code> or <code>--</code>. <b>It is recommended to use a <code>debugName</code> because the short variant is used for debug output in default.</b></td>
<td><code>-v</code> <code>--verbose</code></td>
</tr>
<tr>
<td>Key-Value Argument</td>
<td>An argument to define by a short or long key with marker like <code>-</code> or <code>--</code> and a value assigner like <code>=</code>. With this type of argument you can set specific values. <b>It is recommended to use a <code>debugName</code> because the short variant is used for debug output in default.</b></td>
<td><code>-o=a/b/c</code> <code>--out=a/b/c</code></td>
</tr>
<tr>
<td>Value Argument</td>
<td>An argument with a raw value without any key. The engine checks only if it is not an option argument, start not with like <code>-</code> or <code>--</code>. <b>Please note that you should use a <code>debugName</code> for this argument because there are no key value for debugging output.</b></td>
<td><code>project.prj</code></td>
</tr>
</table> 

### Group Types

There are 3 group types with different handling:

<table>
<tr>
<td>Name</td>
<td>Description</td>
</tr>
<tr>
<td>Any Of</td>
<td>In this group only any of the arguments should be fitting. If no one or more than one of the arguments are fitting it returns in an exception.</td>
</tr>
<tr>
<td>All Of</td>
<td>In this group all the listed arguments must be fitting. If one argument is not found it results in an exception.</td>
</tr>
<tr>
<td>Optional Of</td>
<td>This group describes optional arguments. If one or more arguments are not found it is OK.</td>
</tr>
</table>

Generally, you can use multiple groups in one builder. For example if you combine `AnyOf` 
with `OptionalOf` these results in a must argument followed by optional arguments. See
example below.

### Exception Handling

If there is a problem with reading arguments you must catch the `ArgumentReaderException`
to handle the problem:

```Kotlin
try {
    reader.readArguments("...")
} catch(e: ArgumentReaderException) {
    //handle
}
```

### Argument Instances

The argument instances represent the found arguments are associated with the original 
argument descriptors from the builder. If the argument contains a value you can access
it via property or getter.

Because the instances are resulted as list you must check manually what type of argument
instance it is. See example below.

## Usage

The base class you start is `ArgumentReaderBuilder` to create an `ArgumentReader`. With this reader
you can parse all arguments in the list based on your profile. Additionally, you can print out a help
page based on the profile. 

### Create a reader profile

```Kotlin
ArgumentReaderBuilder.create {
    anyOf("command") {
        description = "Command to run"

        commandArgument("build") {
            description = "Build a complete project"

            arguments {
                allOf("arguments") {
                    valueArgument {
                        description = "project file to build"
                    }
                }
                optionalOf("build options") {
                    keyValueArgument {
                        shortKey = "o"
                        longKey = "out"
                        description = "Output directory for build"
                    }
                }
            }
        }
        commandArgument("run") {
            description = "Run a build project"

            arguments {
                allOf("arguments") {
                    valueArgument {
                        description = "Builded project to run"
                    }
                }
                optionalOf("run options") {
                    optionArgument {
                        shortKey = "d"
                        longKey = "debug"
                        description = "Enable debug mode"
                    }
                }
            }
        }
    }
    optionalOf("common options") {
        optionArgument {
            shortKey = "v"
            longKey = "verbose"
            description = "Verbose logging output"
        }
    }
}.build()
```

This example is a profile for this syntax:

`cli <command> [options]`

`cli build my-project -o=root -v`

`cli run my-project -d -v`

You can see that `-v` is a common option and is valid for all commands. The other arguments are 
specific by command type.

The help page looks like:

```
Syntax: cli build <arguments> [-o=<path> -v]
        cli run <arguments> [-d -v]
        
BUILD
Build a complete project

<arguments>             project file to build
-o=, --out=<path>       Output directory for build

RUN
Run a build project

<arguments>             Builded project to run
-d, --debug             Enable debug mode

COMMONS

-v, --verbose           Verbose logging output
```

### Read the values

To read the values you must call the reader. You get a list of `ArgumentInstance` with all set
arguments and these values.

```Kotlin
val argInstances = complexReader.readArguments("build", "my-project", "-o=root", "-v")

Assertions.assertEquals(4, argInstances.size)

Assertions.assertTrue(argInstances[0] is CommandArgumentInstance, argInstances[0]::class.simpleName)
Assertions.assertEquals("build", argInstances[0].descriptor.key)

Assertions.assertTrue(argInstances[1] is ValueArgumentInstance, argInstances[1]::class.simpleName)
Assertions.assertEquals("my-project", (argInstances[1] as ValueArgumentInstance).value)

Assertions.assertTrue(argInstances[2] is KeyValueArgumentInstance, argInstances[2]::class.simpleName)
Assertions.assertEquals("o", argInstances[2].descriptor.key)
Assertions.assertEquals("root", (argInstances[2] as KeyValueArgumentInstance).value)

Assertions.assertTrue(argInstances[3] is OptionArgumentInstance, argInstances[3]::class.simpleName)
Assertions.assertEquals("v", argInstances[3].descriptor.key)
```