package org.pcsoft.framework.karg.utils

internal val shortLongKeyNullException: IllegalStateException =
    IllegalStateException("Neither shortKey nor longKey was set")