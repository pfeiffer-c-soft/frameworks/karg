package org.pcsoft.framework.karg

import org.pcsoft.framework.karg.args.descriptors.*
import org.pcsoft.framework.karg.args.groups.AllOfArgumentGroup
import org.pcsoft.framework.karg.args.groups.AnyOfArgumentGroup
import org.pcsoft.framework.karg.args.groups.ArgumentGroup
import org.pcsoft.framework.karg.args.groups.OptionalOfArgumentGroup
import org.pcsoft.framework.karg.args.instances.*
import org.pcsoft.framework.karg.utils.logger
import org.pcsoft.framework.karg.utils.shortLongKeyNullException
import java.util.concurrent.atomic.AtomicInteger

class ArgumentReader internal constructor(
    private val shortOptionKey: String,
    private val longOptionKey: String,
    private val optionAssigner: String,
    private val ignoreCase: Boolean,
    private val groups: List<ArgumentGroup>
) {
    val log = logger()

    fun printHelp(short: Boolean = true) {

    }

    fun readArguments(vararg args: String): List<ArgumentInstance<*>> {
        log.info("Start reading arguments: ${args.joinToString()}")

        val result = handleGroups(groups, 0, args)
        return result.argumentInstances
    }

    private fun handleGroups(groups: List<ArgumentGroup>, index: Int, args: Array<out String>): GroupResult {
        val argumentInstances = mutableListOf<ArgumentInstance<*>>()
        var currentIndex = index
        for (group in groups) {
            log.debug("{$currentIndex} [${group.name}] start reading group and its arguments")

            val result = handleGroup(group, currentIndex, args)
            currentIndex = result.newIndex
            argumentInstances.addAll(result.argumentInstances)
        }

        return GroupResult(currentIndex, argumentInstances)
    }

    private fun handleGroup(group: ArgumentGroup, index: Int, args: Array<out String>): GroupResult =
        when (group) {
            is AnyOfArgumentGroup -> handleAnyOf(group, index, args)
            is AllOfArgumentGroup -> handleAllOf(group, index, args)
            is OptionalOfArgumentGroup -> handleOptionalOf(group, index, args)
            else -> throw NotImplementedError("Unknown group type ${group::class.simpleName}")
        }

    private fun handleAnyOf(group: AnyOfArgumentGroup, index: Int, args: Array<out String>): GroupResult {
        log.trace("{$index} [${group.name}] > handle as 'anyOf'")
        return handleDescriptors(group, index, args, true) {
            if (it.isEmpty())
                throw ArgumentReaderException(
                    "For argument group ${group.name} was no fitting argument found - " +
                            "one of these expected: ${group.descriptors.joinToString { it.key }}"
                )
        }
    }

    private fun handleAllOf(group: AllOfArgumentGroup, index: Int, args: Array<out String>): GroupResult {
        log.trace("{$index} [${group.name}] > handle as 'allOf'")
        return handleDescriptors(group, index, args, false)
    }

    private fun handleOptionalOf(group: OptionalOfArgumentGroup, index: Int, args: Array<out String>): GroupResult {
        log.trace("{$index} [${group.name}] > handle as 'optionalOf'")
        return handleDescriptors(group, index, args, true)
    }

    private fun handleDescriptors(
        group: ArgumentGroup, index: Int, args: Array<out String>, ignoreException: Boolean,
        verifyInstances: (List<ArgumentInstance<*>>) -> Unit = {}
    ): GroupResult {
        val argumentInstances = mutableListOf<ArgumentInstance<*>>()
        val currentIndex = AtomicInteger(index)
        for (descriptor in group.descriptors) {
            try {
                log.debug("{{}} [{}] <{}> start reading argument", currentIndex, group.name, descriptor.debugName)

                val result = handleDescriptor(group, descriptor, currentIndex.get(), args)
                currentIndex.set(result.newIndex)
                argumentInstances.addAll(result.argumentInstances)
            } catch (e: ArgumentReaderException) {
                if (ignoreException && !e.primary)
                    continue

                throw e
            }
        }

        verifyInstances(argumentInstances)
        return GroupResult(currentIndex.get(), argumentInstances)
    }

    private fun handleDescriptor(
        group: ArgumentGroup,
        descriptor: ArgumentDescriptor,
        index: Int,
        args: Array<out String>
    ): ArgumentResult =
        when (descriptor) {
            is CommandArgumentDescriptor -> handleCommand(group, descriptor, index, args)
            is KeyValueArgumentDescriptor -> handleKeyValue(group, descriptor, index, args)
            is OptionArgumentDescriptor -> handleOption(group, descriptor, index, args)
            is ValueArgumentDescriptor -> handleValue(group, descriptor, index, args)
            else -> throw NotImplementedError("Unknown argument type ${descriptor::class.simpleName}")
        }

    private fun handleCommand(
        group: ArgumentGroup,
        descriptor: CommandArgumentDescriptor,
        index: Int,
        args: Array<out String>
    ): ArgumentResult {
        log.trace("{$index} [${group.debugName}] <${descriptor.debugName}> > handle as 'command'")

        checkArgumentsCount(index, args, descriptor.key)

        val arg = args[index]
        log.debug(
            "{$index} [${group.debugName}] <${descriptor.debugName}> " +
                    "> check argument $arg to MUST value '${descriptor.key}'"
        )
        if (!arg.equals(descriptor.key, ignoreCase)) {
            log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> failed")
            throw ArgumentReaderException("Required argument ${descriptor.key} not found")
        }
        log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> success")

        val argumentInstance = CommandArgumentInstance(descriptor)
        return handleDescriptorSubGroups(group, descriptor, index + 1, args, argumentInstance)
    }

    private fun handleKeyValue(
        group: ArgumentGroup,
        descriptor: KeyValueArgumentDescriptor,
        index: Int,
        args: Array<out String>
    ): ArgumentResult {
        log.trace("{$index} [${group.debugName}] <${descriptor.debugName}> > handle as 'key-value'")

        checkArgumentsCount(index, args, descriptor.fullKey)

        val arg = args[index]
        log.debug(
            "{$index} [${group.debugName}] <${descriptor.debugName}> " +
                    "> check argument $arg to MUST value {}",
            arrayOf(shortOptionKey + descriptor.shortKey, longOptionKey + descriptor.longKey)
                .filter { it != shortOptionKey && it != longOptionKey }
                .joinToString { "'$it'" }
        )
        if (!arg.contains(optionAssigner))
            throw ArgumentReaderException("Unable to find value assigner $optionAssigner in argument $arg", true)
        if ((descriptor.shortKey != null && !arg.startsWith(
                shortOptionKey + descriptor.shortKey + optionAssigner,
                ignoreCase
            )) &&
            (descriptor.longKey != null && !arg.startsWith(
                longOptionKey + descriptor.longKey + optionAssigner,
                ignoreCase
            ))
        ) {
            log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> failed")
            throw ArgumentReaderException("Required argument ${descriptor.fullKey} not found")
        }

        val value = arg.split(optionAssigner).getOrNull(1)
            ?: throw IllegalStateException("Unreachable exception: Cannot find value assigner $optionAssigner in argument $arg")
        log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> success with value '$value'")

        val argumentInstance = KeyValueArgumentInstance(descriptor, value)
        return handleDescriptorSubGroups(group, descriptor, index + 1, args, argumentInstance)
    }

    private fun handleOption(
        group: ArgumentGroup,
        descriptor: OptionArgumentDescriptor,
        index: Int,
        args: Array<out String>
    ): ArgumentResult {
        log.trace("{$index} [${group.debugName}] <${descriptor.debugName}> > handle as 'option'")

        checkArgumentsCount(index, args, descriptor.fullKey)

        val arg = args[index]
        log.debug(
            "{$index} [${group.debugName}] <${descriptor.debugName}> " +
                    "> check argument $arg to MUST value {}",
            arrayOf(shortOptionKey + descriptor.shortKey, longOptionKey + descriptor.longKey)
                .filter { it != shortOptionKey && it != longOptionKey }
                .joinToString { "'$it'" }
        )
        if ((descriptor.shortKey != null && !arg.startsWith(shortOptionKey + descriptor.shortKey, ignoreCase)) &&
            (descriptor.longKey != null && !arg.startsWith(longOptionKey + descriptor.longKey, ignoreCase))
        ) {
            log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> failed")
            throw ArgumentReaderException("Required argument ${descriptor.fullKey} not found")
        }
        log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> success")

        val argumentInstance = OptionArgumentInstance(descriptor)
        return handleDescriptorSubGroups(group, descriptor, index + 1, args, argumentInstance)
    }

    private fun handleValue(
        group: ArgumentGroup,
        descriptor: ValueArgumentDescriptor,
        index: Int,
        args: Array<out String>
    ): ArgumentResult {
        log.trace("{$index} [${group.debugName}] <${descriptor.debugName}> > handle as 'value'")

        checkArgumentsCount(index, args, descriptor.key)

        val arg = args[index]
        log.debug(
            "{$index} [${group.debugName}] <${descriptor.debugName}> " +
                    "> check argument $arg, all except options is allowed"
        )
        if (arg.startsWith(shortOptionKey) || arg.startsWith(longOptionKey)) {
            log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> failed")
            throw ArgumentReaderException("Required argument ${descriptor.debugName} not found")
        }
        log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> >>> success with value '$arg'")

        val argumentInstance = ValueArgumentInstance(descriptor, arg)
        return handleDescriptorSubGroups(group, descriptor, index + 1, args, argumentInstance)
    }

    private fun handleDescriptorSubGroups(
        group: ArgumentGroup,
        descriptor: ArgumentDescriptor,
        index: Int,
        args: Array<out String>,
        argumentInstance: ArgumentInstance<*>
    ): ArgumentResult {
        if (descriptor.groups.isNotEmpty()) {
            log.debug(">".repeat(100))
            log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> Start reading sub groups: ${descriptor.groups.size}")
            log.debug(">".repeat(100))

            val result = try {
                handleGroups(descriptor.groups, index, args)
            } catch (e: ArgumentReaderException) {
                //Change to primary exception cause argument already fits. So it is a problem
                //in this subgroup of arguments and not a problem with this argument itself
                throw ArgumentReaderException(e.message ?: "", true)
            } finally {
                log.debug("<".repeat(100))
                log.debug("{$index} [${group.debugName}] <${descriptor.debugName}> Finish reading sub groups: ${descriptor.groups.size}")
                log.debug("<".repeat(100))
            }

            val list = result.argumentInstances.toMutableList()
            list.add(0, argumentInstance)

            return ArgumentResult(result.newIndex, list.toList())
        }

        return ArgumentResult(index, listOf(argumentInstance))
    }

    private fun checkArgumentsCount(index: Int, args: Array<out String>, key: String) {
        if (index >= args.size)
            throw ArgumentReaderException("Required argument $key not found: no more arguments")
    }

    private val KeyValueArgumentDescriptor.fullKey: String
        get() = if (this.shortKey != null) shortOptionKey + this.shortKey else
            if (this.longKey != null) longOptionKey + this.longKey else
                throw shortLongKeyNullException

    private val OptionArgumentDescriptor.fullKey: String
        get() = if (this.shortKey != null) shortOptionKey + this.shortKey else
            if (this.longKey != null) longOptionKey + this.longKey else
                throw shortLongKeyNullException

    private data class ArgumentResult(
        val newIndex: Int,
        val argumentInstances: List<ArgumentInstance<*>>
    )

    private data class GroupResult(
        val newIndex: Int,
        val argumentInstances: List<ArgumentInstance<*>>
    )
}

class ArgumentReaderException private constructor(
    val primary: Boolean, message: String?, cause: Throwable?
): Exception(message, cause) {
    constructor(message: String?, primary: Boolean = false) : this(primary, message, null)
}