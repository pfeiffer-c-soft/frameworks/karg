package org.pcsoft.framework.karg.args.groups

import org.pcsoft.framework.karg.args.descriptors.ArgumentDescriptor

data class AnyOfArgumentGroup(
    override val name: String,
    val debug: String?,
    override val description: String,
    override val descriptors: List<ArgumentDescriptor>
) : ArgumentGroup {
    override val debugName: String
        get() = debug ?: name
}
