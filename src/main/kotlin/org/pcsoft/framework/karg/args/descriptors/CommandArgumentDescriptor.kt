package org.pcsoft.framework.karg.args.descriptors

import org.pcsoft.framework.karg.args.groups.ArgumentGroup
import org.pcsoft.framework.karg.builder.CommandArgumentBuilder

data class CommandArgumentDescriptor(
    override val key: String,
    val name: String?,
    override val description: String,
    override val groups: List<ArgumentGroup>
) : ArgumentDescriptor {
    override val debugName: String
        get() = name ?: key

    constructor(builder: CommandArgumentBuilder) : this(builder.key, builder.debugName, builder.description, builder.groups)
}