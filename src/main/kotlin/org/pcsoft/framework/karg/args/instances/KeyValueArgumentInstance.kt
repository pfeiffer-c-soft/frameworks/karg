package org.pcsoft.framework.karg.args.instances

import org.pcsoft.framework.karg.args.descriptors.KeyValueArgumentDescriptor

data class KeyValueArgumentInstance(
    override val descriptor: KeyValueArgumentDescriptor,
    val value: String,
) : ArgumentInstance<KeyValueArgumentDescriptor>