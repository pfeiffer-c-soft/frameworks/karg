package org.pcsoft.framework.karg.args.instances

import org.pcsoft.framework.karg.args.descriptors.ArgumentDescriptor

interface ArgumentInstance<T : ArgumentDescriptor> {
    val descriptor: T
}