package org.pcsoft.framework.karg.args.descriptors

import org.pcsoft.framework.karg.args.groups.ArgumentGroup
import org.pcsoft.framework.karg.builder.ValueArgumentBuilder

data class ValueArgumentDescriptor(
    val name: String?,
    override val description: String,
    override val groups: List<ArgumentGroup>
) : ArgumentDescriptor {
    override val key: String = ""

    override val debugName: String
        get() = name ?: ""

    constructor(builder: ValueArgumentBuilder) : this(builder.debugName, builder.description, builder.groups)

    override fun equals(other: Any?): Boolean {
        return this === other
    }

    override fun hashCode(): Int {
        return System.identityHashCode(this)
    }
}