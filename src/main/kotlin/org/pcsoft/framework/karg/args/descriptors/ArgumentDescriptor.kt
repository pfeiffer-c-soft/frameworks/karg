package org.pcsoft.framework.karg.args.descriptors

import org.pcsoft.framework.karg.args.groups.ArgumentGroup

interface ArgumentDescriptor {
    val key: String
    val debugName: String
    val description: String
    val groups: List<ArgumentGroup>
}