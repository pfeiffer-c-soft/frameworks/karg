package org.pcsoft.framework.karg.args.instances

import org.pcsoft.framework.karg.args.descriptors.OptionArgumentDescriptor

data class OptionArgumentInstance(
    override val descriptor: OptionArgumentDescriptor
) : ArgumentInstance<OptionArgumentDescriptor>