package org.pcsoft.framework.karg.args.groups

import org.pcsoft.framework.karg.args.descriptors.ArgumentDescriptor

interface ArgumentGroup {
    val name: String
    val debugName: String
    val description: String
    val descriptors: List<ArgumentDescriptor>
}