package org.pcsoft.framework.karg.args.descriptors

import org.pcsoft.framework.karg.args.groups.ArgumentGroup
import org.pcsoft.framework.karg.builder.OptionArgumentBuilder
import org.pcsoft.framework.karg.utils.shortLongKeyNullException

data class OptionArgumentDescriptor(
    val shortKey: String?,
    val longKey: String?,
    val name: String?,
    override val description: String,
    override val groups: List<ArgumentGroup>
) : ArgumentDescriptor {
    override val key: String
        get() = shortKey ?: longKey ?: throw shortLongKeyNullException

    override val debugName: String
        get() = name ?: key

    constructor(builder: OptionArgumentBuilder) : this(
        builder.shortKey,
        builder.longKey,
        builder.debugName,
        builder.description,
        builder.groups
    )
}