package org.pcsoft.framework.karg.args.instances

import org.pcsoft.framework.karg.args.descriptors.ValueArgumentDescriptor

data class ValueArgumentInstance(
    override val descriptor: ValueArgumentDescriptor,
    val value: String,
) : ArgumentInstance<ValueArgumentDescriptor>