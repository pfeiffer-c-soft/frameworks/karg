package org.pcsoft.framework.karg.args.instances

import org.pcsoft.framework.karg.args.descriptors.CommandArgumentDescriptor

data class CommandArgumentInstance(
    override val descriptor: CommandArgumentDescriptor
) : ArgumentInstance<CommandArgumentDescriptor>