package org.pcsoft.framework.karg.builder

import org.pcsoft.framework.karg.args.descriptors.*

class ArgumentGroupBuilder {
    private val argumentDescriptors: MutableList<ArgumentDescriptor> = mutableListOf()

    internal val descriptors: List<ArgumentDescriptor> get() = argumentDescriptors.toList()
    var debugName: String? = null
    var description: String = "TBD"

    fun commandArgument(key: String, config: CommandArgumentBuilder.() -> Unit = {}): ArgumentGroupBuilder {
        val builder = CommandArgumentBuilder(key).apply(config)
        argumentDescriptors.add(CommandArgumentDescriptor(builder))

        return this
    }

    fun optionArgument(config: OptionArgumentBuilder.() -> Unit = {}): ArgumentGroupBuilder {
        val builder = OptionArgumentBuilder().apply(config)
        argumentDescriptors.add(OptionArgumentDescriptor(builder))

        return this
    }

    fun keyValueArgument(config: KeyValueArgumentBuilder.() -> Unit = {}): ArgumentGroupBuilder {
        val builder = KeyValueArgumentBuilder().apply(config)
        argumentDescriptors.add(KeyValueArgumentDescriptor(builder))

        return this
    }

    fun valueArgument(config: ValueArgumentBuilder.() -> Unit = {}): ArgumentGroupBuilder {
        val builder: ValueArgumentBuilder = ValueArgumentBuilder().apply(config)
        argumentDescriptors.add(ValueArgumentDescriptor(builder))

        return this
    }
}