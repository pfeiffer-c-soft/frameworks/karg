package org.pcsoft.framework.karg.builder

import org.pcsoft.framework.karg.args.groups.ArgumentGroup

sealed class ArgumentBuilder(
    open val key: String,
) {
    var debugName: String? = null
    var description: String = "TBD"
    private val argumentGroups: MutableList<ArgumentGroup> = mutableListOf()

    val groups: List<ArgumentGroup> get() = argumentGroups.toList()

    fun arguments(config: ArgumentSystemBuilderImpl.() -> Unit) {
        argumentGroups.addAll(ArgumentSystemBuilderImpl().also(config).groups)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ArgumentBuilder) return false

        if (key != other.key) return false

        return true
    }

    override fun hashCode(): Int {
        return key.hashCode()
    }

}

class CommandArgumentBuilder(
    key: String
) : ArgumentBuilder(key)

class OptionArgumentBuilder : ArgumentBuilder("") {
    var shortKey: String? = null
    var longKey: String? = null
    override val key: String
        get() = shortKey ?: longKey ?: throw IllegalStateException("Neither shortKey nor longKey was set")

}

class KeyValueArgumentBuilder : ArgumentBuilder("") {
    var shortKey: String? = null
    var longKey: String? = null
    override val key: String
        get() = shortKey ?: longKey ?: throw IllegalStateException("Neither shortKey nor longKey was set")

}

class ValueArgumentBuilder : ArgumentBuilder("")