package org.pcsoft.framework.karg.builder

import org.pcsoft.framework.karg.args.groups.AllOfArgumentGroup
import org.pcsoft.framework.karg.args.groups.AnyOfArgumentGroup
import org.pcsoft.framework.karg.args.groups.ArgumentGroup
import org.pcsoft.framework.karg.args.groups.OptionalOfArgumentGroup

interface ArgumentSystemBuilder<T : ArgumentSystemBuilder<T>> {
    fun anyOf(name: String, action: ArgumentGroupBuilder.() -> Unit): T
    fun allOf(name: String, action: ArgumentGroupBuilder.() -> Unit): T
    fun optionalOf(name: String, action: ArgumentGroupBuilder.() -> Unit): T
}

class ArgumentSystemBuilderImpl : ArgumentSystemBuilder<ArgumentSystemBuilderImpl> {
    private val argumentGroups: MutableList<ArgumentGroup> = mutableListOf()

    val groups: List<ArgumentGroup> get() = argumentGroups.toList()

    override fun anyOf(name: String, action: ArgumentGroupBuilder.() -> Unit): ArgumentSystemBuilderImpl {
        val builder = ArgumentGroupBuilder().apply(action)
        argumentGroups.add(AnyOfArgumentGroup(name, builder.debugName, builder.description, builder.descriptors))

        return this
    }

    override fun allOf(name: String, action: ArgumentGroupBuilder.() -> Unit): ArgumentSystemBuilderImpl {
        val builder = ArgumentGroupBuilder().apply(action)
        argumentGroups.add(AllOfArgumentGroup(name, builder.debugName, builder.description, builder.descriptors))

        return this
    }

    override fun optionalOf(name: String, action: ArgumentGroupBuilder.() -> Unit): ArgumentSystemBuilderImpl {
        val builder = ArgumentGroupBuilder().apply(action)
        argumentGroups.add(OptionalOfArgumentGroup(name, builder.debugName, builder.description, builder.descriptors))

        return this
    }
}