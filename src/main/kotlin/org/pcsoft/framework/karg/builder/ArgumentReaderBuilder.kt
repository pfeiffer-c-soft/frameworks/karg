package org.pcsoft.framework.karg.builder

import org.pcsoft.framework.karg.ArgumentReader

class ArgumentReaderBuilder private constructor(
    private val shortOptionKey: String,
    private val longOptionKey: String,
    private val optionAssigner: String,
    private val ignoreCase: Boolean,
    private val argumentSystemBuilder: ArgumentSystemBuilderImpl
) : ArgumentSystemBuilder<ArgumentSystemBuilderImpl> by argumentSystemBuilder {

    companion object {
        fun create(shortOptionKey: String = "-", longOptionKey: String = "--", optionAssigner: String = "=",
                   ignoreCase: Boolean = true): ArgumentReaderBuilder =
            ArgumentReaderBuilder(shortOptionKey, longOptionKey, optionAssigner, ignoreCase, ArgumentSystemBuilderImpl())

        fun create(shortOptionKey: String = "-", longOptionKey: String = "--", optionAssigner: String = "=",
                   ignoreCase: Boolean = true, config: ArgumentReaderBuilder.() -> Unit): ArgumentReaderBuilder =
            ArgumentReaderBuilder(shortOptionKey, longOptionKey, optionAssigner, ignoreCase, ArgumentSystemBuilderImpl()).apply(config)
    }

    fun build(): ArgumentReader {
        return ArgumentReader(shortOptionKey, longOptionKey, optionAssigner, ignoreCase, argumentSystemBuilder.groups)
    }

}

