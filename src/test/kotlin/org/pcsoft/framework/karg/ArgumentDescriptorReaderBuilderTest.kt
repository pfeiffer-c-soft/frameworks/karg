package org.pcsoft.framework.karg

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.pcsoft.framework.karg.args.instances.CommandArgumentInstance
import org.pcsoft.framework.karg.args.instances.KeyValueArgumentInstance
import org.pcsoft.framework.karg.args.instances.OptionArgumentInstance
import org.pcsoft.framework.karg.args.instances.ValueArgumentInstance
import org.pcsoft.framework.karg.builder.ArgumentReaderBuilder

class ArgumentDescriptorReaderBuilderTest {
    companion object {
        private val reader = ArgumentReaderBuilder.create {
            anyOf("command") {
                description = "Command to run"

                commandArgument("build") {
                    description = "Build a complete project"

                    arguments {
                        allOf("arguments") {
                            valueArgument {
                                debugName ="project-file"
                                description = "project file to build"
                            }
                        }
                        optionalOf("build options") {
                            keyValueArgument {
                                shortKey = "o"
                                longKey = "out"
                                debugName = "out"
                                description = "Output directory for build"
                            }
                        }
                    }
                }
                commandArgument("run") {
                    description = "Run a build project"

                    arguments {
                        allOf("arguments") {
                            valueArgument {
                                debugName = "project-binary"
                                description = "Builded project to run"
                            }
                        }
                        optionalOf("run options") {
                            optionArgument {
                                shortKey = "d"
                                longKey = "debug"
                                debugName = "debug"
                                description = "Enable debug mode"
                            }
                        }
                    }
                }
            }
            optionalOf("common options") {
                optionArgument {
                    shortKey = "v"
                    longKey = "verbose"
                    debugName = "verbose"
                    description = "Verbose logging output"
                }
            }
        }.build()
    }

    @Test
    fun testWithBuild() {
        val argInstances = reader.readArguments("build", "my-project", "-o=root", "-v")

        Assertions.assertEquals(4, argInstances.size)

        Assertions.assertTrue(argInstances[0] is CommandArgumentInstance, argInstances[0]::class.simpleName)
        Assertions.assertEquals("build", argInstances[0].descriptor.key)

        Assertions.assertTrue(argInstances[1] is ValueArgumentInstance, argInstances[1]::class.simpleName)
        Assertions.assertEquals("my-project", (argInstances[1] as ValueArgumentInstance).value)

        Assertions.assertTrue(argInstances[2] is KeyValueArgumentInstance, argInstances[2]::class.simpleName)
        Assertions.assertEquals("o", argInstances[2].descriptor.key)
        Assertions.assertEquals("root", (argInstances[2] as KeyValueArgumentInstance).value)

        Assertions.assertTrue(argInstances[3] is OptionArgumentInstance, argInstances[3]::class.simpleName)
        Assertions.assertEquals("v", argInstances[3].descriptor.key)
    }

    @Test
    fun testWithBuildNoOpt() {
        val argInstances = reader.readArguments("build", "my-project")

        Assertions.assertEquals(2, argInstances.size)

        Assertions.assertTrue(argInstances[0] is CommandArgumentInstance, argInstances[0]::class.simpleName)
        Assertions.assertEquals("build", argInstances[0].descriptor.key)

        Assertions.assertTrue(argInstances[1] is ValueArgumentInstance, argInstances[1]::class.simpleName)
        Assertions.assertEquals("my-project", (argInstances[1] as ValueArgumentInstance).value)
    }

    @Test
    fun testWithRun() {
        val argInstances = reader.readArguments("run", "my-project", "-d", "-v")

        Assertions.assertEquals(4, argInstances.size)

        Assertions.assertTrue(argInstances[0] is CommandArgumentInstance, argInstances[0]::class.simpleName)
        Assertions.assertEquals("run", argInstances[0].descriptor.key)

        Assertions.assertTrue(argInstances[1] is ValueArgumentInstance, argInstances[1]::class.simpleName)
        Assertions.assertEquals("my-project", (argInstances[1] as ValueArgumentInstance).value)

        Assertions.assertTrue(argInstances[2] is OptionArgumentInstance, argInstances[2]::class.simpleName)
        Assertions.assertEquals("d", argInstances[2].descriptor.key)

        Assertions.assertTrue(argInstances[3] is OptionArgumentInstance, argInstances[3]::class.simpleName)
        Assertions.assertEquals("v", argInstances[3].descriptor.key)
    }

    @Test
    fun testWithRunNoOpt() {
        val argInstances = reader.readArguments("run", "my-project")

        Assertions.assertEquals(2, argInstances.size)

        Assertions.assertTrue(argInstances[0] is CommandArgumentInstance, argInstances[0]::class.simpleName)
        Assertions.assertEquals("run", argInstances[0].descriptor.key)

        Assertions.assertTrue(argInstances[1] is ValueArgumentInstance, argInstances[1]::class.simpleName)
        Assertions.assertEquals("my-project", (argInstances[1] as ValueArgumentInstance).value)
    }

    @Test
    fun testWithEmptyArgs() {
        val exception = Assertions.assertThrows(ArgumentReaderException::class.java) {
            reader.readArguments()
        }

        Assertions.assertEquals(
            "For argument group command was no fitting argument found - one of these expected: build, run",
            exception.message
        )
    }

    @Test
    fun testWithWrongCommand() {
        val exception = Assertions.assertThrows(ArgumentReaderException::class.java) {
            reader.readArguments("info")
        }

        Assertions.assertEquals(
            "For argument group command was no fitting argument found - one of these expected: build, run",
            exception.message
        )
    }

    @Test
    fun testWithBuildNoRequired() {
        val exception = Assertions.assertThrows(ArgumentReaderException::class.java) {
            reader.readArguments("build", "-v")
        }

        Assertions.assertEquals(
            "Required argument project-file not found",
            exception.message
        )
    }

    @Test
    fun testWithRunNoRequired() {
        val exception = Assertions.assertThrows(ArgumentReaderException::class.java) {
            reader.readArguments("run", "-v")
        }

        Assertions.assertEquals(
            "Required argument project-binary not found",
            exception.message
        )
    }

}